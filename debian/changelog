kworkflow (1:0.4-1) experimental; urgency=medium

  * Epoch bump to use upstream' s new versioning system
  * debian/control:
    - Add Rules-Require-Root field.
    - Add Vcs-Git and Vcs-Browser fields.
    - Add myself as uploader
    - Change architecture from any to all.
    - Set debhelper-compat version in Build-Depends and update it to 13.
    - Update dependencies and suggestions
  * debian/copyright:
    - Fix field name: Upstream-contact => Upstream-Contact.
    - Use spaces rather than tabs to start continuation lines.
  * debian/patches:
    - Remove obsolete patches.
    - Add patch 0001: Fix file permissions.
      Add patch 0002: Adjust directory structure in upstream.
  * debian/rules:
    - convert upstream's rst to manpage.
    - generate html documentation with sphinx.
  * debian/tests:
    - Add allow-stderr to avoid false negatives.
    - Add missing dependencies.
    - Use fake TERM environmental variable to trick the tput command inside
      upstream's test suite.
  * debian/upstream/metadata: Add Bug-Database, Bug-Submit, Repository and
                              Repository-Browse fields.
  * Add debian/install.
  * Create debian/manpages.
  * Remove debian/kworkflow.links.

  [ Steve Langasek ]
  * debian/tests: add git default branch option. (Closes: #980575)

 -- João Seckler <jseckler@riseup.net>  Sat, 03 Apr 2021 12:41:30 -0300

kworkflow (20191112-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Switch b-d from python-docutils to python3-docutils; Closes: #951822

 -- Sandro Tosi <morph@debian.org>  Sat, 30 May 2020 02:37:01 -0400

kworkflow (20191112-1) unstable; urgency=low

  * Initial release. Closes: #946781

 -- Rodrigo Carvalho <rodrigorsdc@gmail.com>  Thu, 12 Dec 2019 14:19:30 -0300
